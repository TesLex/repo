#!/bin/bash

__artefact_name="$1"
__artefact_version="$2"

case "$__artefact_version" in  
     *\ * )
		 echo "version should not contain spaces"
		 exit
		 ;;
esac


__readme_file="README.md"

__date=$(date | sed 's/ /_/g')

gawk -f update_readme.awk \
	-i inplace \
	-v artefact_name="$__artefact_name:" \
	-v artefact_version="$__artefact_version" \
	-v date="$__date" \
	"$__readme_file"
