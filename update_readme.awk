$1 == "-" && $2 == artefact_name {
	$3 = "`" artefact_version "`"
}

$1 == "**UPDATED**:" {
	$2 = "`" date "`"
}

{print}
